#include "filesys/dirstorage.h"

#include <cassert>

namespace infosaur::filesys
{

using namespace detail;

namespace
{

constexpr auto defaultCapacity = 128 * 1024;

constexpr auto nullIndex = FileIndex{0};

bool isValid(FileIndex index)
{
    return index != nullIndex;
}

template <typename Tag>
inline FileIndex unwrap(StrongIndex<Tag> index)
{
    return index.m_value;
}

}

DirStorage::DirStorage()
{
    m_parent.reserve(defaultCapacity);
    m_child.reserve(defaultCapacity);
    m_next.reserve(defaultCapacity);
    m_name.reserve(defaultCapacity);
    add(ParentIndex{0}, ChildIndex{0}, NextIndex{0}, "");
}

DirStorage::FileIndex DirStorage::find(FileIndex parent, const std::string &name) const
{
    for (auto ch = getChild(parent); ch != nullIndex; ch = getNext(ch))
    {
        if (getName(ch) == name)
        {
            return ch;
        }
    }
    return nullIndex;
}

DirStorage::FileIndex DirStorage::findOrAdd(FileIndex parent, const std::string &name)
{
    auto ch = getChild(parent);
    if (!isValid(ch))
    {
        const auto newIndex = add(ParentIndex{parent}, {}, {}, name);
        m_child[parent] = newIndex;
        return newIndex;
    }

    FileIndex prev;
    do
    {
        if (getName(ch) == name)
        {
            return ch;
        }
        prev = ch;
        ch = getNext(ch);
    } while (isValid(ch));

    const auto newIndex = add(ParentIndex{parent}, {}, {}, name);
    m_next[prev] = newIndex;
    return newIndex;
}

FileIndex DirStorage::add(FileIndex parent, const std::string &name)
{
    auto ch = getChild(parent);
    if (!isValid(ch))
    {
        const auto newIndex = add(ParentIndex{parent}, {}, {}, name);
        m_child[parent] = newIndex;
        return newIndex;
    }

    FileIndex prev;
    do
    {
        assert(getName(ch) != name);
        prev = ch;
        ch = getNext(ch);
    } while (isValid(ch));

    const auto newIndex = add(ParentIndex{parent}, {}, {}, name);
    m_next[prev] = newIndex;
    return newIndex;
}

FileIndex DirStorage::getParent(FileIndex index) const
{
    return m_parent[index];
}

FileIndex DirStorage::getChild(FileIndex index) const
{
    return m_child[index];
}

FileIndex DirStorage::getNext(DirStorage::FileIndex index) const
{
    return m_next[index];
}

const std::string &DirStorage::getName(FileIndex index) const
{
    return m_name[index];
}

DirStorage::FileIndex DirStorage::size() const
{
    return m_parent.size();
}

FileIndex DirStorage::add(ParentIndex parent, ChildIndex child, NextIndex next, const std::string &name)
{
    const auto newIndex = size();
    m_parent.push_back(unwrap(parent));
    m_child.push_back(unwrap(child));
    m_next.push_back(unwrap(next));
    m_name.push_back(name);
    return newIndex;
}



}
