#include "filesys/dirtask.h"

namespace infosaur::filesys
{

DirTask::DirTask(std::string absoluteDirPath)
    : m_absoluteDirPath(absoluteDirPath)
    , m_directoryIterator(absoluteDirPath)
{
}

void DirTask::process()
{

}

}
