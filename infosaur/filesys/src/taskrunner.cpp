#include "filesys/taskrunner.h"

namespace infosaur::filesys
{

TaskRunner::TaskRunner()
{
    m_thread = std::thread([this]{
        threadLoop();
    });
}

TaskRunner::~TaskRunner()
{
    {
        m_terminate = true;
        std::unique_lock<std::mutex> lock(m_mutex);
        m_condition.notify_all();
    }

    if (m_thread.joinable())
    {
        m_thread.join();
    }
}

void TaskRunner::addTask(TaskPtr task)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    task->m_seqNr = ++m_seqNr;
    m_queue.push_front(std::move(task));
    m_condition.notify_all();
}

void TaskRunner::sleep()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    if (!m_terminate)
    {
        m_condition.wait(lock);
    }
}

void TaskRunner::threadLoop()
{
    do
    {
        Task *runningTask = nullptr;
        {
            std::unique_lock<std::mutex> lock(m_mutex);

            if (m_terminate)
            {
                break;
            }

            for (auto &task : m_queue)
            {
                if (task->m_state != TaskState::Finished)
                {
                    runningTask = task.get();
                    break;
                }
            }

            if (runningTask == nullptr)
            {
                m_condition.wait(lock);
            }
        }

        if (runningTask)
        {
            runningTask->m_state = TaskState::Running;
            while (!m_terminate && runningTask->m_seqNr == m_seqNr && runningTask->m_state != TaskState::Finished)
            {
                runningTask->process();
            }
            if (runningTask->m_state == TaskState::Finished)
            {
                // TODO:
            }
        }
    }
    while (!m_terminate);
}

}
