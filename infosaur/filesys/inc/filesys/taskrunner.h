#pragma once

#include "task.h"

#include <deque>
#include <optional>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <queue>


namespace infosaur::filesys
{

class TaskRunner
{
    using TaskQueue = std::deque<TaskPtr>;
    using AtomicTaskPtr = std::atomic<Task *>;
    static_assert (AtomicTaskPtr::is_always_lock_free);

public:
    TaskRunner();
    ~TaskRunner();
    void addTask(TaskPtr task);

private:
    void sleep();
    void threadLoop();

    std::mutex m_mutex;
    std::condition_variable m_condition;
    std::atomic_bool m_terminate{false};
    std::atomic_int m_seqNr{};
    TaskQueue m_queue;
    AtomicTaskPtr m_currentTask;
    std::thread m_thread;

};

}
