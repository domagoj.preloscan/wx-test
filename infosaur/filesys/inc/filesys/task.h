#pragma once

#include <atomic>
#include <memory>

namespace infosaur::filesys
{

enum class TaskState
{
    Pending,
    Running,
    Finished
};

class Task
{
    friend class TaskRunner;
public:
    virtual void process() = 0;

protected:
    std::atomic<TaskState> m_state{TaskState::Pending};
    int m_seqNr{};
};

using TaskPtr = std::unique_ptr<Task>;

}
