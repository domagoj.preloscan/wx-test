#pragma once

#include "task.h"
#include <string>
#include <filesystem>

namespace infosaur::filesys
{

class DirTask : public Task
{
public:
    DirTask(std::string absoluteDirPath);
    void process() override;

private:
    std::string m_absoluteDirPath;
    std::filesystem::directory_iterator m_directoryIterator;
};

}
