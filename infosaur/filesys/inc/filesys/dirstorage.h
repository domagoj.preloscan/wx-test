#pragma once

#include <vector>
#include <string>

namespace infosaur::filesys
{

namespace detail
{

using FileIndex = std::uint32_t;

template <typename Tag>
struct StrongIndex
{
    StrongIndex() : m_value(0) {}
    explicit StrongIndex(FileIndex value) : m_value(value) {}
    FileIndex m_value;
};


}

class DirStorage
{
    using FileIndex = detail::FileIndex;


    template <typename T>
    using Container = std::vector<T>;

//    using IndexStorage = std::vector<Index>;
    using NameStorage = std::vector<std::string>;

    using ParentIndex = detail::StrongIndex<struct ParentIndexTag>;
    using ChildIndex = detail::StrongIndex<struct ChildIndexTag>;
    using NextIndex = detail::StrongIndex<struct NextIndexTag>;

public:

    DirStorage();

    FileIndex find(FileIndex parent, const std::string &name) const;
    FileIndex findOrAdd(FileIndex parent, const std::string &name);
    FileIndex add(FileIndex parent, const std::string &name);

    FileIndex getParent(FileIndex index) const;
    FileIndex getChild(FileIndex index) const;
    FileIndex getNext(FileIndex index) const;
    const std::string &getName(FileIndex index) const;

private:
    FileIndex size() const;
    FileIndex add(ParentIndex parent, ChildIndex child, NextIndex next, const std::string &name);
    //void add(ParentIndex parent, const std::string &name);

//    Container<ParentIndex> m_parent;
//    Container<ChildIndex> m_child;
//    Container<NextIndex> m_next;
    Container<FileIndex> m_parent;
    Container<FileIndex> m_child;
    Container<FileIndex> m_next;
    NameStorage m_name;
};

}
