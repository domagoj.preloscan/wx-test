#pragma once

#include <vector>
#include <array>
#include <stdexcept>
#include <span>
#include<algorithm>

namespace infosaur::filehandling
{

template <typename T, std::uint32_t ChunkBits>
class ChunkedBuffer
{
public:
    using BufferType = ChunkedBuffer<T, ChunkBits>;
    using ValueType = T;
    using Index = std::uint32_t;
    using Size = Index;
    using Span = std::span<const T>;

    static constexpr auto ChunkSize = 1 << ChunkBits;
    static constexpr auto IndexMask = ChunkSize -1;
    static constexpr auto ChunkMask = ~IndexMask;
    static_assert(IndexMask != 0);

    struct Cursor
    {
        Cursor(int index) : m_index(index)
        {}

        operator std::size_t() const
        {
            return m_index;
        }

        Index chunk() const
        {
            return (m_index & ChunkMask) >> ChunkBits;
        }

        Index index() const
        {
            return m_index & IndexMask;
        }

        Index m_index;
    };

    Size allocatedSize() const
    {
        const auto size = m_data.size() * ChunkSize * sizeof(T);
        return size;
    }

    Size size() const
    {
        return m_size;
    }

    Size capacity() const
    {
        return m_data.size() * ChunkSize;
    }

    T& at(Cursor position)
    {
        if (position >= size())
        {
            throw std::out_of_range("ChunkedBuffer index is larger than allocated buffer size");
        }
        return m_data.at(position.chunk()).at(position.index());
    }

    const T& at(Cursor position) const
    {
        if (position >= size())
        {
            throw std::out_of_range("ChunkedBuffer index is larger than allocated buffer size");
        }
        return m_data.at(position.chunk()).at(position.index());
    }

    T& back()
    {
        return at(size() - 1);
    }

    const T& back() const
    {
        return at(size() - 1);
    }

    Cursor begin() const
    {
        return Cursor(0);
    }

    Cursor end() const
    {
        return Cursor(m_size);
    }

    void pushBack(T&& element)
    {
        resize(size() + 1);
        back() = element;
    }

    Cursor append(Span from)
    {
        const Cursor insertedPos = end();
        while (from.size() > 0)
        {
            const auto lastChunkSize = end().index();
            const auto lastChunk = end().chunk();
            const auto freeChunkCapacity = ChunkSize - lastChunkSize;
            const auto toCopy = std::min<std::size_t>(from.size(), freeChunkCapacity);
            const auto remaining = from.size() - toCopy;
            m_data.resize(lastChunk + 1);
            auto& chunk = m_data[lastChunk];
            std::copy_n(from.data(), toCopy, chunk.data() + lastChunkSize);
            m_size += toCopy;
            from = from.last(remaining);
        }
        return insertedPos;
    }

    Cursor append(const BufferType &fromBuffer)
    {
        const Cursor insertedPos = end();

        const Cursor newSize = size() + fromBuffer.size();
        reserve(newSize);

        Cursor from = fromBuffer.begin();
        Size size = fromBuffer.size();

        while (size > 0)
        {
            const auto fromElements = std::min(size, ChunkSize - from.index());
            const auto toElements = std::min(size, remaingChunkCapacity());
            const auto toCopy = std::min(fromElements, toElements);
            std::copy_n(fromBuffer.m_data.at(from.chunk()).data() + from.index(), toCopy,  appendLocation());
            size -= toCopy;
            m_size += toCopy;
            from = from + toCopy;
        }
        return insertedPos;
    }

    auto fetch(Cursor from, Size size, auto &toContainer) const
    {
        toContainer.clear();
        toContainer.reserve(size);
        while (size > 0)
        {
            const auto chunkElements = ChunkSize - from.index();
            const auto toCopy = std::min<std::size_t>(size, chunkElements);
            toContainer.append(m_data.at(from.chunk()).data() + from.index(), toCopy);
            from = from + toCopy;
            size -= toCopy;
        }
        return std::span<T>(toContainer);
    }

    void clear()
    {
        m_data.clear();
        m_size = 0;
    }

    void reserve(Size size)
    {
        const auto chunks = Cursor(size + ChunkSize - 1).chunk();
        const std::size_t newChunks = std::max<std::size_t>(m_data.size(), chunks);
        m_data.resize(newChunks);
    }

    void resize(Size size)
    {
        const std::size_t chunks = Cursor(size + ChunkSize - 1).chunk();
        m_data.resize(chunks);
        m_size = size;
    }

private:
    Size remaingChunkCapacity() const
    {
        return ChunkSize - end().index();
    }

    T* appendLocation()
    {
        return m_data.at(end().chunk()).data() + end().index();
    }

    using  Data = std::vector<std::array<T, ChunkSize>>;
    Data m_data;
    Size m_size = 0;
};

}
