#pragma once

#include "filehandling/chunkedbuffer.h"

#include <string>
#include <vector>
#include <optional>
#include <cassert>

namespace infosaur::filehandling
{

class StringStorage
{
public:
    using Size = std::uint32_t;
    using Index = std::uint32_t;

    Size getSize() const;
    Size allocatedSize() const;

    std::string_view fetchString(Index index, std::string &destination) const;
    void clear();
    void append(std::string_view string);
    void append(const StringStorage &other);

private:
    using Buffer = ChunkedBuffer<char, 10>;
    using Indices = ChunkedBuffer<Index, 10>;
    Buffer m_data;
    Indices m_indices;
};

class DirEntry
{
public:
    using Entry = std::uint32_t;
    using Index = Entry;

    static constexpr Entry   lastDirMask{0x80000000};
    static constexpr Entry     indexMask{0x7FFFFFFF};
    static constexpr Entry     unvisited{0x00000000};
    static constexpr Entry lastUnvisited{0x80000000};
    static constexpr Entry         empty{0x7FFFFFFF};
    static constexpr Entry     lastEmpty{0xFFFFFFFF};

    explicit constexpr DirEntry(Entry entry) : m_entry(entry) {}
    DirEntry withSubDirIndex(Index subDir) const;
    std::optional<Index> firstSubDirIndex() const;
    bool isLast() const;
    bool isUnvisited() const;
    bool isEmpty() const;
    bool hasChildren() const;

private:
    Index getSubDirField() const;
    Entry m_entry;
};

using DirEntries = std::vector<DirEntry>;

struct DirCursor
{
    DirCursor(const DirEntries& dirs, DirEntry::Index dirIndex);

    DirEntry::Index m_index;
    DirEntry m_entry;
};

class FileData
{
public:

    using Size = std::uint32_t;
    using Index = DirEntry::Index;

    static constexpr DirEntry unvisitedDir{DirEntry::unvisited};
    static constexpr DirEntry lastUnvisitedDir{DirEntry::lastUnvisited};
    static constexpr Size unknownSize = Size(-1);
    static constexpr Index rootIndex{0};

    Size getSubDirCountRecursive(DirCursor parentDir) const;

    explicit FileData(std::string_view root);
    void clear();
    void setEmptyDir(DirCursor dir);
    std::optional<DirCursor> addSubDirectories(DirCursor parentDirectory, const StringStorage &subDirNames);
    DirCursor dirCursor(Index index) const;
    std::optional<DirCursor> nextDir(DirCursor cursor) const;
    DirCursor root() const;
    std::string_view fetchName(DirCursor dir, std::string &to) const;
    std::string makeDirTree() const;
    Size dirStructureSize() const;
    Size dirNamesSize() const;
    Size allocatedSize() const;

private:
    DirEntries m_dirs;
    StringStorage m_dirNames;
};

}
