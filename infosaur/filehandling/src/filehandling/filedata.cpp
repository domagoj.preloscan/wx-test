#include "filehandling/filedata.h"

namespace infosaur::filehandling
{

namespace
{
void visitDirTree(const FileData &fileData, DirCursor topDir, auto &&Fnc)
{
    const auto visitDirRecursive = [&](const auto &self, DirCursor dir, int level) -> void {
        // for this directory
        Fnc(dir, level);

        // for sub-directories
        if (const auto firstSubDirIndex = dir.m_entry.firstSubDirIndex(); firstSubDirIndex)
        {
            for (auto subDirIndex = *firstSubDirIndex; ; ++subDirIndex)
            {
                DirCursor child = fileData.dirCursor(subDirIndex);
                self(self, child, level + 1);
                if (child.m_entry.isLast())
                {
                    break;
                }
            }
        }
    };

    visitDirRecursive(visitDirRecursive, topDir, 0);
}
}

StringStorage::Size StringStorage::getSize() const
{
    return m_indices.size();
}

StringStorage::Size StringStorage::allocatedSize() const
{
    return m_indices.allocatedSize() + m_data.allocatedSize();
}

std::string_view StringStorage::fetchString(Index index, std::string &destination) const
{
    const auto beginIndex = m_indices.at(index);
    const auto endIndex = index + 1 == m_indices.size() ? m_data.size() : m_indices.at(index + 1);
    assert(beginIndex <= m_data.size());
    assert(endIndex <= m_data.size());
    m_data.fetch(beginIndex, endIndex - beginIndex, destination);
    return destination;
}

void StringStorage::clear()
{
    m_data.clear();
    m_indices.clear();
}

void StringStorage::append(std::string_view string)
{
    m_indices.pushBack(m_data.size());
    m_data.append(string);
}

void StringStorage::append(const StringStorage &other)
{
    const Index offset = m_data.size();
    m_indices.reserve(m_indices.size() + other.m_indices.size());
    for (auto i = 0; i < other.m_indices.size(); ++i)
    {
        m_indices.pushBack(other.m_indices.at(i) + offset);
    }
    m_data.append(other.m_data);
}

DirEntry DirEntry::withSubDirIndex(Index subDir) const
{
    return DirEntry{(m_entry & lastDirMask) | subDir};
}

bool DirEntry::isLast() const
{
    return m_entry & lastDirMask;
}

bool DirEntry::isUnvisited() const
{
    return getSubDirField() == unvisited;
}

bool DirEntry::isEmpty() const
{
    return getSubDirField() == empty;
}

bool DirEntry::hasChildren() const
{
    return !isUnvisited() && !isEmpty();
}

DirEntry::Index DirEntry::getSubDirField() const
{
    return m_entry & indexMask;
}

std::optional<DirEntry::Index> DirEntry::firstSubDirIndex() const
{
    if (hasChildren())
    {
        return getSubDirField();
    }
    else
    {
        return std::nullopt;
    }
}


DirCursor::DirCursor(const DirEntries &dirs, DirEntry::Index dirIndex)
    : m_index(dirIndex)
    , m_entry(dirs.at(dirIndex))
{
}


FileData::Size FileData::getSubDirCountRecursive(DirCursor parentDir) const
{
    Size count = -1;
    visitDirTree(*this, root(), [&](DirCursor dir, int level){
        ++count;
    });
    return count;
}

FileData::FileData(std::string_view root)
{
   m_dirNames.append(root);
   m_dirs.push_back(lastUnvisitedDir);
}

void FileData::setEmptyDir(DirCursor dir)
{
    DirEntry &entry = m_dirs.at(dir.m_index);
    entry = entry.withSubDirIndex(DirEntry::empty);
}

std::optional<DirCursor> FileData::addSubDirectories(DirCursor parentDirectory, const StringStorage &subDirNames)
{
    DirEntry &parent = m_dirs.at(parentDirectory.m_index);
    const auto addCount = subDirNames.getSize();
    if (addCount == 0)
    {
        parent = parent.withSubDirIndex(DirEntry::empty);
        return std::nullopt;
    }
    else
    {
        const Index insertionIndex = m_dirs.size();
        parent = parent.withSubDirIndex(insertionIndex);
        m_dirs.resize(m_dirs.size() + addCount, unvisitedDir);
        m_dirs.back() = lastUnvisitedDir;
        m_dirNames.append(subDirNames);
        return dirCursor(insertionIndex);
    }
}

DirCursor FileData::dirCursor(Index index) const
{
    return DirCursor{m_dirs, index};
}

std::optional<DirCursor> FileData::nextDir(DirCursor currentDir) const
{
    if (currentDir.m_entry.isLast())
    {
        return std::nullopt;
    }
    else
    {
        return dirCursor(currentDir.m_index + 1);
    }
}

DirCursor FileData::root() const
{
    return dirCursor(rootIndex);
}

std::string_view FileData::fetchName(DirCursor dir, std::string &to) const
{
    return m_dirNames.fetchString(dir.m_index, to);
}

std::string FileData::makeDirTree() const
{
    std::string result;
    std::string padding(128, ' ');
    std::string tmp;

    visitDirTree(*this, root(), [&](DirCursor dir, int level){
        padding.resize(level * 2, ' ');
        result += padding;
        m_dirNames.fetchString(dir.m_index, tmp);
        result += tmp;
        if (dir.m_entry.isUnvisited())
        {
            result += " *\n";
        }
        else
        {
            result += "\n";
        }
    });

    return result;
}

FileData::Size FileData::dirStructureSize() const
{
    return m_dirs.size() * sizeof(DirEntry);
}

FileData::Size FileData::dirNamesSize() const
{
    return m_dirNames.allocatedSize();
}

FileData::Size FileData::allocatedSize() const
{
    return dirStructureSize() + dirNamesSize();
}

}
