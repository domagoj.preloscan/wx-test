#include "filehandling/filedata.h"
#include "filehandling/chunkedbuffer.h"

#include <cassert>
#include <iostream>
#include <chrono>
#include <string_view>

namespace infosaur::test::filehandling
{

using namespace infosaur::filehandling;

namespace {

void mustThrow(auto &&fnc)
{
    try {
        fnc();
        assert(false && "the function did not throw");
    }
    catch (...)
    {
    }
}

StringStorage makeStrings(std::initializer_list<std::string_view> names)
{
    StringStorage ss;
    for (const auto &name : names)
    {
        ss.append(name);
    }
    return ss;
}

auto chars(std::string_view view)
{
    return std::span<const char>(view.data(), view.length());
}

template <typename T>
double numeric(auto dur)
{
    return duration_cast<std::chrono::duration<double, T>>(dur).count();
}

}


void ChunkedBuffer_default()
{
    using Buffer = ChunkedBuffer<char, 2>;
    static_assert(Buffer::ChunkSize == 4);
    static_assert(Buffer::ChunkMask == 0xFFFFFFFC);
    static_assert(Buffer::IndexMask == 0x00000003);
    Buffer buffer;
    assert(buffer.size() == 0);
    assert(buffer.capacity() == 0);
    mustThrow([&]{auto x = buffer.at(buffer.begin());});
}

void ChunkedBuffer_appendEmpty()
{
    using Buffer = ChunkedBuffer<char, 2>;
    Buffer buffer;
    buffer.append(chars(""));
    assert(buffer.size() == 0);
    assert(buffer.capacity() == 0);
    mustThrow([&]{auto x = buffer.at(0);});
}

void ChunkedBuffer_appendSmall()
{
    using Buffer = ChunkedBuffer<char, 2>;
    Buffer buffer;
    //
    buffer.append(chars("abc"));
    assert(buffer.size() == 3);
    assert(buffer.capacity() == 4);
    assert(buffer.at(0) == 'a');
    assert(buffer.at(1) == 'b');
    assert(buffer.at(2) == 'c');
    mustThrow([&]{auto x = buffer.at(3);});
    //
    buffer.append(chars("wxyz"));
    assert(buffer.size() == 7);
    assert(buffer.capacity() == 8);
    assert(buffer.at(0) == 'a');
    assert(buffer.at(1) == 'b');
    assert(buffer.at(2) == 'c');
    assert(buffer.at(3) == 'w');
    assert(buffer.at(4) == 'x');
    assert(buffer.at(5) == 'y');
    assert(buffer.at(6) == 'z');
    mustThrow([&]{auto x = buffer.at(7);});
}

void ChunkedBuffer_appendLarge()
{
    using Buffer = ChunkedBuffer<char, 2>;
    Buffer buffer;
    // add 5
    buffer.append(chars("abcde"));
    assert(buffer.size() == 5);
    assert(buffer.capacity() == 8);
    assert(buffer.at(0) == 'a');
    assert(buffer.at(1) == 'b');
    assert(buffer.at(2) == 'c');
    assert(buffer.at(3) == 'd');
    assert(buffer.at(4) == 'e');
    mustThrow([&]{auto x = buffer.at(5);});
    // add 9
    buffer.append(chars("567890123"));
    assert(buffer.size() == 14);
    assert(buffer.capacity() == 16);
    assert(buffer.at(0) == 'a');
    assert(buffer.at(4) == 'e');
    assert(buffer.at(5) == '5');
    assert(buffer.at(7) == '7');
    assert(buffer.at(8) == '8');
    assert(buffer.at(13) == '3');
    mustThrow([&]{auto x = buffer.at(14);});
}

void ChunkedBuffer_fetch()
{
    using Buffer = ChunkedBuffer<char, 2>;
    Buffer buffer;
    buffer.append(chars("abc"));
    std::string fetched;
    buffer.fetch(0, 3, fetched);
    assert(fetched == "abc");
    buffer.fetch(0, 0, fetched);
    assert(fetched == "");
    buffer.fetch(0, 1, fetched);
    assert(fetched == "a");
    buffer.fetch(1, 2, fetched);
    assert(fetched == "bc");
    buffer.append(chars("wxyz"));
    buffer.fetch(2, 3, fetched);
    assert(fetched == "cwx");
    buffer.fetch(buffer.begin(), buffer.size(), fetched);
    assert(fetched == "abcwxyz");
}

void ChunkedBuffer_tests()
{
    std::cout << "BEGIN ChunkedBuffe tests" << std::endl;
    ChunkedBuffer_default();
    ChunkedBuffer_appendEmpty();
    ChunkedBuffer_appendSmall();
    ChunkedBuffer_appendLarge();
    ChunkedBuffer_fetch();
    std::cout << "END   ChunkedBuffe tests" << std::endl;
}

void StringStorage_defaults()
{
    StringStorage ss;
    std::string tmp;
    assert(ss.getSize() == 0);
    mustThrow([&]{ss.fetchString(0, tmp);});
}

void StringStorage_appendEmpty()
{
    StringStorage ss;
    std::string tmp;
    ss.append("");
    assert(ss.getSize() == 1);
    assert(ss.fetchString(0, tmp) == "");
    mustThrow([&]{ss.fetchString(1, tmp);});

    ss.append("");
    assert(ss.getSize() == 2);
    assert(ss.fetchString(0, tmp) == "");
    assert(ss.fetchString(1, tmp) == "");
    mustThrow([&]{ss.fetchString(2, tmp);});
}

void StringStorage_append()
{
    StringStorage ss;
    std::string tmp;
    ss.append("abc");
    assert(ss.getSize() == 1);
    assert(ss.fetchString(0, tmp) == "abc");
    mustThrow([&]{ss.fetchString(1, tmp);});

    ss.append("wxyz");
    assert(ss.getSize() == 2);
    assert(ss.fetchString(0, tmp) == "abc");
    assert(ss.fetchString(1, tmp) == "wxyz");
    mustThrow([&]{ss.fetchString(2, tmp);});
}

void StringStorage_tests()
{
    std::cout << "BEGIN StringStorage tests" << std::endl;
    StringStorage_defaults();
    StringStorage_appendEmpty();
    StringStorage_append();
    std::cout << "END   StringStorage tests" << std::endl;
}

void FileData_defaults()
{
    FileData fd("/");
    assert(fd.getSubDirCountRecursive(fd.root()) == 0);
}

void FileData_append()
{
    FileData fd("/");
    const auto a = fd.addSubDirectories(fd.root(), makeStrings({"a", "b", "c"}));
    fd.addSubDirectories(*a, makeStrings({"a1", "a2", "a3"}));
    const auto b = fd.nextDir(*a);
    fd.addSubDirectories(*b, makeStrings({"b1", "b2", "b3", "b4"}));
    const auto c = fd.nextDir(*b);
    fd.addSubDirectories(*c, makeStrings({"c1", "c2"}));
    assert(fd.getSubDirCountRecursive(fd.root()) == 12);
}

void FileData_addManyDirs()
{
    FileData fd("/");
    StringStorage newNames;
    std::string dirName;
    std::string tmp;

    const auto addDirs = [&](const auto &self, DirCursor parentDir) ->void {
        newNames.clear();
        dirName = fd.fetchName(parentDir, tmp);
        dirName += 'x';
        if (dirName.length() <= 7)
        {
            for (auto c = '0'; c <= '9'; ++c)
            {
                dirName.back() = c;
                newNames.append(dirName);
            }
            const auto firstChild = fd.addSubDirectories(parentDir, newNames);
            for (auto child = firstChild; child; child = fd.nextDir(*child))
            {
                self(self, *child);
            }
        }
        else
        {
            fd.setEmptyDir(parentDir);
        }
    };

    using namespace std::chrono;
    const auto t0 = steady_clock::now();
    newNames.append("d");
    const auto testDir = fd.addSubDirectories(fd.root(), newNames);
    addDirs(addDirs, *testDir);
    const auto t1 = steady_clock::now();

    const auto dt = t1 - t0;
    const auto subDirCount = fd.getSubDirCountRecursive(fd.root());
    const auto timeMs = numeric<std::milli>(dt);
    const auto timePerDirNs = numeric<std::nano>(dt / subDirCount);
    std::cout << "Dir count: " << subDirCount << "  time: " << timeMs << " ms  time/dir: " << timePerDirNs << " ns" << std::endl;
    std::cout << "Dir structure allocated bytes: " << fd.dirStructureSize() << std::endl;
    std::cout << "Dir names allocated bytes: " << fd.dirNamesSize() << std::endl;
    std::cout << "Dir total allocated bytes: " << fd.allocatedSize() << std::endl;

    const auto tree = fd.makeDirTree();
    std::cout << tree.substr(0, 200) << "\n...\n" << tree.substr(tree.length() - 100) << std::endl;
}

void FileData_tests()
{
    std::cout << "BEGIN FileData tests" << std::endl;
    FileData_defaults();
    FileData_append();
    FileData_addManyDirs();
    std::cout << "END   FileData tests" << std::endl;
}

}

using namespace infosaur::test::filehandling;

int main()
{
    ChunkedBuffer_tests();
    StringStorage_tests();
    FileData_tests();
}

