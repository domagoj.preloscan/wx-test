#pragma once

#include "wx/wx.h"
#include <memory>

namespace infosaur::filesaur
{

enum class ProcessingState
{
    Empty,
    Processing,
    Done
};

template <typename T>
struct StatefulValue
{
    T& operator->()
    {
        return m_value;
    }

    void setValue(T &&value)
    {
        m_value = value;
        m_state = ProcessingState::Done;
    }

    void clear()
    {
        m_value = T{};
        m_state = ProcessingState::Empty;
    }

    bool isEmpty() const
    {
        return m_state == ProcessingState::Empty;
    }

    bool isProcessing() const
    {
        return m_state == ProcessingState::Processing;
    }

    bool isFinished() const
    {
        return m_state == ProcessingState::Done;
    }

    T m_value{};
    ProcessingState m_state{ProcessingState::Empty};
};

struct ImageData
{
    ImageData(std::string dirPath, std::string fileName)
        : m_dirPath(std::move(dirPath))
        , m_fileName(std::move(fileName))
    {
    }

    std::string m_dirPath;
    std::string m_fileName;
    StatefulValue<wxImage> m_loadedImage;
    StatefulValue<wxBitmap> m_normalBitmap;
    StatefulValue<wxBitmap> m_highQualityBitmap;
};

using ImageDataPtr = std::shared_ptr<ImageData>;

}
