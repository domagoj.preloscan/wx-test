#pragma once

#include "imagedata.h"
#include "wx/wx.h"
#include <mutex>

namespace infosaur::filesaur
{

class ImagePanel : public wxPanel
{
public:
    ImagePanel(wxWindow* parent);
    void showImage(wxImage image);

    void setImageData(ImageDataPtr imageData);

private:
    void paintEvent(wxPaintEvent &evt);

    wxImage m_image;
    ImageDataPtr m_imageData;
    std::mutex m_mutex;
};

}
