#pragma once

#include "imagedata.h"
#include "wx/wx.h"
#include <deque>
#include <optional>
#include <mutex>
#include <condition_variable>
#include <thread>

namespace infosaur::filesaur
{

class ImageHandler
{
    using Images = std::deque<ImageDataPtr>;
    using ImageDataFnc = std::function<void(ImageDataPtr imageData)>;

public:
    explicit ImageHandler(ImageDataFnc callback);
    ~ImageHandler();
    void setBitmapSize(wxSize bmpSize);
    void processImage(std::string dirPath, std::string fileName, std::size_t position);
    void storeImage(std::string dirPath, std::string fileName, wxImage image);
    void storeNormalBitmap(std::string dirPath, std::string fileName, wxBitmap bitmap);
    void storeHighQualityBitmap(std::string dirPath, std::string fileName, wxBitmap bitmap);

private:
    std::optional<std::size_t> findImageIndex(const std::string &dirPath, const std::string &fileName);
    void runImageLoader();
    void runImageProcessor();

    ImageDataFnc m_callback;
    Images m_images;
    std::mutex m_mutex;
    std::condition_variable m_condition;
    bool m_terminate{false};
    std::thread m_imageLoader;
    std::thread m_imageProcessor;
    wxSize m_bitmapSize;
};

}
