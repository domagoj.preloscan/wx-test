#pragma once

#include "imagepanel.h"
#include "filetabledata.h"
#include "imagehandler.h"
#include "filesys/dirstorage.h"
#include "wx/wx.h"
#include <wx/grid.h>

namespace infosaur::filesaur
{

class MainFrame : public wxFrame
{
public:
    MainFrame(const wxString& title, const wxSize& size);

private:
    void showDirectory();
    void showImage(int row);
    void processImageData(ImageDataPtr imageData);

    ImageHandler m_imageHandler;
    FileTableData *m_fileTableData;
    wxPanel *m_panel;
    wxTextCtrl *m_leftPath;
    wxTextCtrl *m_rightPath;
    ImagePanel *m_imagePanel;
    wxGrid *m_fileGrid;
    std::mutex m_mutex;
    std::string m_dirPath;
    std::string m_fileName;

    filesys::DirStorage m_toDo;
};

}
