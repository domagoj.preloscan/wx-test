#pragma once

#include "wx/wx.h"
#include <wx/grid.h>

namespace infosaur::filesaur
{

class FileTableData : public wxGridTableBase
{
public:
    FileTableData();
    void setFileNames(std::vector<std::string> fileNames);

    // wxGridTableBase interface
public:
    int GetNumberRows() override;
    int GetNumberCols() override;
    wxString GetValue(int row, int col) override;
    void SetValue(int row, int col, const wxString &value) override;

    bool InsertRows( size_t pos = 0, size_t numRows = 1 ) override;
    bool AppendRows( size_t numRows = 1 ) override;
    bool DeleteRows( size_t pos = 0, size_t numRows = 1 ) override;

    virtual wxString GetColLabelValue( int col ) override;
//    virtual void SetColLabelValue( int col, const wxString& value) override;


    std::vector<std::string> m_fileNames; // dummy
};

}
