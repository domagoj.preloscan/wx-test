#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "mainframe.h"

#include<filesystem>

namespace infosaur::filesaur
{

namespace
{

template <class T>
auto *makePanelWithColour(wxWindow *parent, T colour)
{
    wxColour panelColour;
    panelColour.Set(colour);
    auto *panel = new wxPanel(parent, wxID_ANY);
    panel->SetBackgroundColour(panelColour);
    return panel;
}

std::filesystem::path normalizedPath(const wxString &p)
{
    return std::filesystem::absolute(p.ToStdString());
}

}

MainFrame::MainFrame(const wxString& title, const wxSize& size)
    : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, size)
    , m_imageHandler([this](ImageDataPtr imageData){
        processImageData(imageData);
        })
{
    wxInitAllImageHandlers();

    m_panel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize);

//    m_leftPath = new wxTextCtrl(m_panel, wxID_ANY, normalizedPath("C:/moje/foto/pocophone/Camera").string());
//    m_leftPath = new wxTextCtrl(m_panel, wxID_ANY, normalizedPath("../../wx-test/filesaur/res"));
    m_leftPath = new wxTextCtrl(m_panel, wxID_ANY, normalizedPath(".").string());
    auto *leftBrowse = new wxButton(m_panel, wxID_ANY, wxT("Browse"));
    leftBrowse->Bind(wxEVT_COMMAND_BUTTON_CLICKED, [this](auto &event){
        wxDirDialog dlg(m_panel, "Open Left Directory", normalizedPath(m_leftPath->GetValue()).string(), wxDD_DEFAULT_STYLE);
        if (dlg.ShowModal() == wxID_OK)
        {
            m_leftPath->SetValue(normalizedPath(dlg.GetPath()).string());
            showDirectory();
        }
    });

    m_rightPath = new wxTextCtrl(m_panel, wxID_ANY, "/home/example/right");

    m_fileGrid = new wxGrid(m_panel, wxID_ANY);
    m_fileTableData = new FileTableData;
    m_fileGrid->SetTable(m_fileTableData, true);
    m_fileGrid->HideRowLabels();
//    m_fileGrid->SetColLabelValue(0, "File Name");
    m_fileGrid->SetMinSize({200, wxDefaultCoord});
    m_fileGrid->SetCellHighlightColour({0, 100, 0});
    m_fileGrid->EnableEditing(false);


    m_imagePanel = new ImagePanel(m_panel);

    auto *topSizer = new wxBoxSizer(wxHORIZONTAL);
    topSizer->Add(m_leftPath, 1, wxEXPAND | wxALL, 2);
    topSizer->Add(leftBrowse, 0, wxALL, 2);
    topSizer->Add(m_rightPath, 1, wxEXPAND | wxALL, 2);

    auto *mainSizer = new wxBoxSizer(wxHORIZONTAL);
    mainSizer->Add(m_fileGrid, 0, wxEXPAND | wxSHRINK | wxALL, 1);
    mainSizer->Add(m_imagePanel, 1, wxEXPAND | wxSHRINK | wxALL, 1);

    auto *sizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(topSizer, 0, wxEXPAND | wxALL, 1);
    sizer->Add(mainSizer, 1, wxEXPAND | wxSHRINK | wxALL, 1);

    m_panel->SetSizerAndFit(sizer);

    Centre();

    m_fileGrid->SetColSize(0, m_fileGrid->GetClientSize().GetWidth());

    m_fileGrid->Bind(wxEVT_GRID_SELECT_CELL, [this](wxGridEvent &event){
        showImage(event.GetRow());
    });

    m_imagePanel->Bind(wxEVT_SIZE, [this](wxSizeEvent &event){
        auto sz = event.GetSize();
        m_imageHandler.setBitmapSize(sz);
        event.Skip(true);
    });

    showDirectory();
}

void MainFrame::showDirectory()
{
    std::filesystem::path dir(m_leftPath->GetValue().ToStdString());
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_dirPath = dir.string();
    }

    std::vector<std::string> files;
    for(auto& p: std::filesystem::directory_iterator(dir))
    {
        std::cout << p.path() << std::endl;
        if (p.is_regular_file())
        {
            files.push_back(p.path().filename().string());
        }
    }

    m_fileTableData->setFileNames(files);
    m_fileGrid->GoToCell(0, 0);
    m_fileGrid->SetFocus();
}

void MainFrame::showImage(int row)
{
    if (row < m_fileGrid->GetNumberRows())
    {
        const auto fileName = m_fileGrid->GetCellValue(row, 0).ToStdString();
        m_rightPath->SetValue(fileName);
        if (!fileName.empty())
        {
            std::string dirPath;
            {
                std::unique_lock<std::mutex> lock(m_mutex);
                m_fileName = fileName;
                dirPath = m_dirPath;
            }
            m_imageHandler.processImage(dirPath, fileName, 0);
        }
    }
}

void MainFrame::processImageData(ImageDataPtr imageData)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    if (m_dirPath == imageData->m_dirPath && m_fileName == imageData->m_fileName)
    {
        m_imagePanel->setImageData(imageData);
    }
}

}
