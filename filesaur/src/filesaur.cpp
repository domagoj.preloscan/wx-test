#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "filesaur.h"
#include "mainframe.h"

namespace infosaur::filesaur
{

class FilesaurApp: public wxApp
{
public:
    virtual bool OnInit() override;
    virtual bool OnExceptionInMainLoop() override;
    virtual void OnUnhandledException() override;
};

bool FilesaurApp::OnInit()
{
    wxLog::EnableLogging(false);
    auto *frame = new MainFrame( "Filesaur", wxSize(450, 340) );
    frame->Show( true );
    return true;
}

bool FilesaurApp::OnExceptionInMainLoop()
{
    return false;
}

void FilesaurApp::OnUnhandledException()
{

}

}

wxIMPLEMENT_APP(infosaur::filesaur::FilesaurApp);
