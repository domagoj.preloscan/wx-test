#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "imagepanel.h"

namespace infosaur::filesaur
{

ImagePanel::ImagePanel(wxWindow* parent) : wxPanel(parent)
{
    Bind(wxEVT_SIZE, [this](wxSizeEvent &evt){
        Refresh();
    });

    Bind(wxEVT_PAINT, &ImagePanel::paintEvent, this);
}

void ImagePanel::showImage(wxImage image)
{
    m_image = std::move(image);
    Refresh();
}

void ImagePanel::setImageData(ImageDataPtr imageData)
{
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_imageData = imageData;
    }
    Refresh();
}

void ImagePanel::paintEvent(wxPaintEvent &evt)
{
    wxPaintDC dc(this);

//    wxPen pen(*wxRED, 1);
//    dc.SetPen(pen);
//    dc.DrawRectangle(10, 20, 30, 40);

    std::unique_lock<std::mutex> lock(m_mutex);
    if (m_imageData && m_imageData->m_normalBitmap.m_value.IsOk())
    {
        dc.DrawBitmap(m_imageData->m_normalBitmap.m_value, 0, 0, false);
    }
}

}
