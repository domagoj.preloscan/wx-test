#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "imagehandler.h"
#include <filesystem>

namespace infosaur::filesaur
{

namespace
{

wxSize proportionalSize(wxSize originalSize, wxSize bmpSize)
{
    const auto factor = std::min(1.0 * bmpSize.GetWidth() / originalSize.GetWidth(),
                                 1.0 * bmpSize.GetHeight() / originalSize.GetHeight());

    return originalSize * factor;
}

}

ImageHandler::ImageHandler(ImageDataFnc callback)
    : m_callback(callback)
{
    m_imageLoader = std::thread([this]{
        runImageLoader();
    });

    m_imageProcessor = std::thread([this]{
        runImageProcessor();
    });
}

ImageHandler::~ImageHandler()
{
    {
        m_terminate = true;
        std::unique_lock<std::mutex> lock(m_mutex);
        m_condition.notify_all();
    }

    if (m_imageLoader.joinable())
    {
        m_imageLoader.join();
    }

    if (m_imageProcessor.joinable())
    {
        m_imageProcessor.join();
    }
}

void ImageHandler::setBitmapSize(wxSize bmpSize)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    m_bitmapSize = bmpSize;
    for (auto &img : m_images)
    {
        img->m_normalBitmap.clear();
    }

    m_condition.notify_all();
}

void ImageHandler::processImage(std::string dirPath, std::string fileName, size_t position)
{
    ImageDataPtr imageData;
    bool hasNormalBitmap = false;

    {
        std::unique_lock<std::mutex> lock(m_mutex);
        const auto idx = findImageIndex(dirPath, fileName);
        if (idx.has_value())
        {
            if (*idx != position)
            {
                imageData = std::move(m_images[*idx]);
                m_images.erase(std::next(m_images.begin(), *idx));
                m_images.push_front(imageData);
            }
            else
            {
                imageData = m_images[*idx];
            }
        }
        else
        {
            imageData = std::make_shared<ImageData>(std::move(dirPath), std::move(fileName));
            m_images.push_front(imageData);
        }

        if (m_images.size() > 100)
        {
            m_images.resize(100);
        }

        hasNormalBitmap = imageData->m_normalBitmap.isFinished();
        m_condition.notify_all();
    }

    if (hasNormalBitmap)
    {
        m_callback(imageData);
    }
}

void ImageHandler::storeImage(std::string dirPath, std::string fileName, wxImage image)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    const auto idx = findImageIndex(dirPath, fileName);
    if (idx.has_value())
    {
        m_images[*idx]->m_loadedImage.setValue(std::move(image));
    }
}

void ImageHandler::storeNormalBitmap(std::string dirPath, std::string fileName, wxBitmap bitmap)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    const auto idx = findImageIndex(dirPath, fileName);
    if (idx.has_value())
    {
        m_images[*idx]->m_normalBitmap.setValue(std::move(bitmap));
    }
}

void ImageHandler::storeHighQualityBitmap(std::string dirPath, std::string fileName, wxBitmap bitmap)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    const auto idx = findImageIndex(dirPath, fileName);
    if (idx.has_value())
    {
        m_images[*idx]->m_highQualityBitmap.setValue(std::move(bitmap));
    }
}

std::optional<size_t> ImageHandler::findImageIndex(const std::string &dirPath, const std::string &fileName)
{
    for (std::size_t i = 0; i < m_images.size(); ++i)
    {
        const auto &img = m_images[i];
        if (img->m_dirPath == dirPath && img->m_fileName == fileName)
        {
            return i;
        }
    }
    return {};
}

void ImageHandler::runImageLoader()
{
    wxLog::EnableLogging(false);
    ImageDataPtr processedImage;
    for (;;)
    {
        {
            std::unique_lock<std::mutex> lock(m_mutex);

            if (m_terminate)
            {
                break;
            }

            m_condition.wait(lock);

            if (m_terminate)
            {
                break;
            }
            else
            {
                for (auto &img : m_images)
                {
                    if (img->m_loadedImage.isEmpty())
                    {
                        processedImage = img;
                        img->m_loadedImage.m_state = ProcessingState::Processing;
                        break;
                    }
                }
            }
        }

        if (processedImage)
        {
            const auto p = std::filesystem::path(processedImage->m_dirPath) / processedImage->m_fileName;
            wxImage img(p.string(), wxBITMAP_TYPE_ANY);
            if (img.IsOk())
            {
                std::unique_lock<std::mutex> lock(m_mutex);
                processedImage->m_loadedImage.setValue(std::move(img));
                processedImage.reset();
                m_condition.notify_all();
            }
        }
    }
}

void ImageHandler::runImageProcessor()
{
    wxLog::EnableLogging(false);
    ImageDataPtr processedImage;
    wxSize bmpSize;
    for (;;)
    {
        {
            std::unique_lock<std::mutex> lock(m_mutex);

            if (m_terminate)
            {
                break;
            }

            m_condition.wait(lock);

            if (m_terminate)
            {
                break;
            }
            else if (m_bitmapSize.GetWidth() > 0 && m_bitmapSize.GetHeight() > 0)
            {
                for (const auto &img : m_images)
                {
                    if (img->m_loadedImage.isFinished() && img->m_normalBitmap.isEmpty())
                    {
                        bmpSize = proportionalSize(img->m_loadedImage.m_value.GetSize(), m_bitmapSize);
                        processedImage = img;
                        img->m_normalBitmap.m_state = ProcessingState::Processing;
                        break;
                    }
                }
            }
        }

        if (processedImage)
        {
            wxBitmap resized(processedImage->m_loadedImage.m_value.Scale(bmpSize.GetWidth(), bmpSize.GetHeight(), wxIMAGE_QUALITY_NORMAL));
            if (resized.IsOk())
            {
                std::unique_lock<std::mutex> lock(m_mutex);
                processedImage->m_normalBitmap.setValue(std::move(resized));
                m_callback(processedImage);
                processedImage.reset();
                m_condition.notify_all();
            }
        }
    }
}

}
