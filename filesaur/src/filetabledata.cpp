#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "filetabledata.h"

namespace infosaur::filesaur
{

FileTableData::FileTableData()
{
    m_fileNames.push_back("empty list");
}

void FileTableData::setFileNames(std::vector<std::string> fileNames)
{
    if ( GetView() )
    {
        wxGridTableMessage msg1( this, wxGRIDTABLE_NOTIFY_ROWS_DELETED,
                                0, m_fileNames.size() );
        m_fileNames.clear();
        GetView()->ProcessTableMessage( msg1 );

        m_fileNames.swap(fileNames);
        wxGridTableMessage msg2( this, wxGRIDTABLE_NOTIFY_ROWS_APPENDED,
                                m_fileNames.size(), 0 );
        GetView()->ProcessTableMessage( msg2 );
    }
}


int FileTableData::GetNumberRows()
{
    return m_fileNames.size();
}

int FileTableData::GetNumberCols()
{
    return 1;
}

wxString FileTableData::GetValue(int row, int col)
{
    if (row >= m_fileNames.size())
    {
        return {};
    }

    return m_fileNames[row];
}

void FileTableData::SetValue(int row, int col, const wxString &value)
{
}

bool FileTableData::InsertRows(size_t pos, size_t numRows)
{
    return true;
}

bool FileTableData::AppendRows(size_t numRows)
{
    return true;
}

bool FileTableData::DeleteRows(size_t pos, size_t numRows)
{
    return true;
}

wxString FileTableData::GetColLabelValue(int col)
{
    return "File Name";
}

//void FileTableData::SetColLabelValue(int col, const wxString &value)
//{

//}

}
